const path = require('path');

module.exports = {
  // entry: ['src/scss/main.scss', 'src/js/index.js'],
  entry: ['src/scss/main.scss'],
  output: {
    format: 'cjs',
    dir: path.resolve(__dirname, 'site/static/assets')
  }
}
