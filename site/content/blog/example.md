---
title: Example post
date: 2019-01-01
author: Lorem Ipsum
tags:
- examples
category:
- posts
---

{{< aside >}}
## ohoo

Nullam venenatis turpis a rhoncus dapibus. Proin posuere pellentesque est gravida pellentesque. Fusce iaculis hendrerit turpis, vitae viverra lacus blandit sit amet. Nam sagittis fermentum nulla, volutpat vestibulum quam placerat vel.
{{< /aside >}}

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nec interdum metus. Aenean rutrum ligula sodales ex auctor, sed tempus dui mollis. Curabitur ipsum dui, aliquet nec commodo at, tristique eget ante. **Donec quis dolor nec nunc mollis interdum vel in purus**. Sed vitae leo scelerisque, sollicitudin elit sed, congue ante. In augue nisl, vestibulum commodo est a, tristique porttitor est. Proin laoreet iaculis ornare. Nullam ut neque quam.

#### h4: a very cool table

| Tables   |      Are      |  Cool |
|:----------|:-------------:|------:|
| col 1 is |  left-aligned | 1600  |
| col 2 is |    centered   |   12  |
| col 3 is | right-aligned |    1  |

Donec dapibus venenatis lacus. In hac habitasse platea dictumst. Maecenas pellentesque feugiat felis ut tempor. Integer eu fringilla magna. Proin at purus pulvinar, molestie massa eget, scelerisque eros. In vitae fermentum turpis. Ut congue iaculis sem, nec interdum est faucibus eu. 

> Fusce pharetra suscipit orci nec tempor. Quisque vitae sem sit amet sem mollis consequat. Sed at imperdiet lorem. Vestibulum pharetra faucibus odio, ac feugiat tellus sollicitudin at. Pellentesque varius tristique mi imperdiet dapibus. Duis orci odio, sodales lacinia venenatis sit amet, feugiat et diam.

[Sed a leo id risus](https://example.com) venenatis vulputate non quis nulla. Aenean nisl quam, lacinia pulvinar orci sit amet, eleifend eleifend dui. Nulla tempor ligula leo, eu vehicula quam condimentum a. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla gravida tristique nunc sed semper. Morbi nec felis odio.

## h2: cool code

{{< highlight css >}}
body {
      background: red;
}
{{< /highlight >}}

- Sed efficitur, lacus ac scelerisque pellentesque, lectus risus dignissim nisl, fermentum semper lectus diam eget lacus.
- Nunc ornare purus enim, id eleifend mauris vestibulum volutpat.
- Aenean facilisis ut ipsum condimentum ultrices.
- Fusce sed metus vulputate, lobortis purus et, finibus purus. Suspendisse quis posuere lorem. Vivamus vulputate nec risus in pulvinar.

# h1: a glorious [picture](//example.com)

{{< figure 
      src="//via.placeholder.com/200x100" 
      link="//files.catbox.moe/9kusrt.png" 
      alt="megumin"
      title="Megumin"
      caption="by Kantoku"
>}}
