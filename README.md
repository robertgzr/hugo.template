hugo.template
=============

Template for static sites using [hugo](https://gohugo.io) + [sass](https://sass-lang.com).

1. Clone this repository
2. Start building your hugo site in `site/`
3. Add js/scss/etc. in `src/`
4. Use `yarn build` to generate a static webpage in `dist/`

## Requirements:

Local development:
- yarn
- hugo

CI:
- docker (to run jojomi/hugo)
